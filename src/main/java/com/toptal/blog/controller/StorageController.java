package com.toptal.blog.controller;

import com.toptal.blog.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@RestController
@RequestMapping(value = "/api/storage")
public class StorageController {

	@Autowired
	private StorageService storageService;

	public StorageController(StorageService storageService) {
		this.storageService = storageService;
	}

	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	@ResponseBody
	public void uploadFile(@RequestParam(value = "file", required = true) MultipartFile file) throws IOException {
		storageService.uploadFile(file);
	}

	@RequestMapping(value = "/download", method = RequestMethod.GET)
	@ResponseBody
	public void downloadFile(@RequestParam(value = "fileName") String fileName, HttpServletResponse response)
			throws IOException {

		InputStream inputStream = storageService.downloadFile(fileName);

		OutputStream outStream = response.getOutputStream();
		byte[] buffer = new byte[4096];
		int bytesRead = -1;

		response.setContentType("application/octet-stream");

		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"", fileName);
		response.setHeader(headerKey, headerValue);

		while ((bytesRead = inputStream.read(buffer)) != -1) {
			outStream.write(buffer, 0, bytesRead);
		}

		inputStream.close();
		outStream.close();

	}

}
